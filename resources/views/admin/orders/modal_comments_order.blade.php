<div class="modal fade" id="modal_comments_order" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h4 class="modal-title">
                    Agregar un comentario a la orden
                </h4>
            </div> -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-fit portlet-form ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-commenting-o font-green"></i>
                                    <span class="caption-subject font-green sbold uppercase">Agregar un comentario al proceso</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="form-body">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <div class="input-group col-md-12">
                                            <textarea class="form-control" id="order_comment" rows="3"></textarea>
                                            <label for="order_comment">
                                                Comentario
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-default btn-block" id="cancel_comment" data-dismiss="modal" aria-label="Close">
                                                Cancelar
                                            </button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn green btn-block btn-flat" data-dismiss="modal">
                                                Guardar Comentario
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>