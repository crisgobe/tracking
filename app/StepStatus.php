<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StepStatus extends Model
{
    protected $table = 'step_status';
}
